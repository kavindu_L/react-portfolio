import FilterBar from "./Components/FilterBar";
import ProjectList from "./Components/ProjectList";
import { useState } from "react";

function ReactPortfolio() {
  const [filterBy, setFilterBy] = useState("react");

  function handleFilter(event) {
    setFilterBy(event.target.value);
  }

  return (
    <div className="container mt-3">
      <div className="bg-light rounded mb-3">
        <center>
          <h2 className="display-4">React Portfolio</h2>
        </center>
      </div>

      <div className="bg-light rounded mb-3">
        <FilterBar handleFilter={handleFilter} />
      </div>

      <div className="overflow-auto" style={{ height: "400px" }}>
        <ProjectList filterBy={filterBy} />
      </div>
    </div>
  );
}

export default ReactPortfolio;
