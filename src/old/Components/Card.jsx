function Card({ project }) {
  return (
    <div className="card m-3" style={{ width: "18rem" }}>
      <div className="card-body">
        <h5 className="card-title">{project.projectTitle}</h5>
        <h6 className="card-subtitle mb-2 text-muted">
          using <i>{project.dependencies.join(", ")}</i>
        </h6>
        <p className="card-text">{project.description}</p>
        <a
          href={project.demoLink}
          className="btn btn-primary btn-sm"
          target="_blank"
          rel="noopener noreferrer"
        >
          {project.projectTitle + " demo"}
        </a>
      </div>
    </div>
  );
}

export default Card;
