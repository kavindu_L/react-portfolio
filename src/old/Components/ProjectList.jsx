import Card from "./Card";
import { projects } from "../data";

function ProjectList({ filterBy }) {
  return (
    <div className="bg-light rounded d-flex flex-wrap">
      {projects
        .filter(project => project.dependencies.includes(filterBy))
        .map(project => (
          <Card project={project} key={project.id} />
        ))}
    </div>
  );
}

export default ProjectList;
