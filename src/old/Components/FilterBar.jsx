import { dependencies } from "../data";

function FilterBar({ handleFilter }) {
  return (
    <div className="py-2 ps-2">
      {dependencies.map((dependency, index) => (
        <button
          className="btn btn-dark btn-sm rounded-pill m-1"
          onClick={e => handleFilter(e)}
          key={index}
          value={dependency}
        >
          {dependency}
        </button>
      ))}
    </div>
  );
}

export default FilterBar;
