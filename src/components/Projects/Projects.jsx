import { BsArrowRight } from "react-icons/bs";

import "./projects.css";
import { projects } from "../../data";

function Projects() {
  return (
    <div className="rp-projects-section" id="projects">
      <center>
        <div className="rp-projects-title">
          <h1>Projects</h1>
        </div>

        <div className="rp-project-list-wrapper">
          <div className="rp-project-container">
            <div className="rp-project-row">
              {projects.map(p => (
                <div className="rp-project-card">
                  <h3>{p.projectTitle}</h3>
                  <div className="rp-projects-demo-link-wrapper">
                    <a href={p.demoLink} target="_blank" rel="noreferrer">
                      <button>
                        <i>See project</i>
                      </button>
                    </a>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>

        <div className="rp-projects-contact-btn">
          <a href="#contact">
            <button>
              Contact me <BsArrowRight />
            </button>
          </a>
        </div>
      </center>
    </div>
  );
}

export default Projects;
