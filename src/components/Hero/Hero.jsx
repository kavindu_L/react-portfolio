import { BsArrowRight } from "react-icons/bs";

import "./hero.css";

function Hero() {
  return (
    <div className="rp-hero-section" id="hero">
      <center>
        <div className="rp-header-wrapper">
          <div className="rp-header-container">
            <div className="rp-nav-link">
              <span className="rp-nav-link-txt">
                <a href="#hero">Home</a>
              </span>
            </div>
            <div className="rp-nav-link">
              <span className="rp-nav-link-txt">
                <a href="#projects">Projects</a>
              </span>
            </div>
            <div className="rp-nav-link">
              <span className="rp-nav-link-txt">
                <a href="#contact">Contact</a>
              </span>
            </div>
          </div>
        </div>

        <div className="rp-hero-body-wrapper">
          <div className="rp-hero-body-content">
            <h1 className="rp-hero-body-heading">React</h1>
            <h1 className="rp-hero-body-heading">Portfolio</h1>
            <p className="rp-hero-body-para">Welcome to my React portfolio,</p>

            <p className="rp-hero-body-para">
              I'm Kavindu Loransuhewa and I'm a software enginer from Colombo,
              Sri Lanka. This is my React portfolio. This includes a few sample
              projects that I have involed with.
            </p>

            <p className="rp-hero-body-para">
              I'm a person who is eager to learn & try new things. I know my
              potentials & I'm clear with what I can and cannot do. Looking
              forward to employ the things I've learnt so as to further enrich
              my expertise.
            </p>
            <div className="rp-hero-body-nxt-btn">
              <a href="#projects">
                <button>
                  Go to projects <BsArrowRight />
                </button>
              </a>
            </div>
          </div>

          <div className="rp-hero-body-img-wrapper">
            <img
              src="https://img.freepik.com/free-vector/hand-drawn-web-developers_23-2148819604.jpg"
              alt="hero-img"
              className="rp-hero-body-img"
            />
          </div>
        </div>
      </center>
    </div>
  );
}

export default Hero;
