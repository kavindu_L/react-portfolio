import { BsLinkedin } from "react-icons/bs";
import { ImBehance2 } from "react-icons/im";

import "./contact.css";

function Contact() {
  return (
    <div className="rp-contact-section" id="contact">
      <center>
        <h1 className="rp-contact-section-title">Contact Me</h1>
        <div className="rp-contact-email-div">
          <p>
            I can be reached at <br />
            <a href="mailto:kloransuhewa@gmail.com">
              <i>kloransuhewa@gmail.com</i>
            </a>
          </p>
          <p>or</p>
        </div>

        <div className="rp-contact-icon">
          <a
            href="https://www.linkedin.com/in/kavindu-loransuhewa"
            target="_blank"
            rel="noreferrer"
          >
            <BsLinkedin
              style={{ width: "100%", height: "100%", color: "white" }}
            />
          </a>
        </div>

        {/* <div className="rp-contact-icon-list">
          <div className="rp-contact-icon">
            <a
              href="https://www.linkedin.com/in/kavindu-loransuhewa"
              target="_blank"
              rel="noreferrer"
            >
              <BsLinkedin style={{ width: "100%", height: "100%" }} />
            </a>
          </div>
        </div> */}

        <div className="rp-contact-more">
          More : <br />
          Here's my art portfolio
          <div className="rp-contact-icon">
            <a
              href="https://www.behance.net/loraayanz"
              target="_blank"
              rel="noreferrer"
            >
              <ImBehance2
                style={{ width: "100%", height: "100%", color: "white" }}
              />
            </a>
          </div>
        </div>
        <div className="rp-contact-top-btn">
          <a href="#hero">
            <button>Top</button>{" "}
          </a>
        </div>
      </center>
    </div>
  );
}

export default Contact;
