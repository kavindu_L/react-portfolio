export const projects = [
  {
    id: 1,
    projectTitle: "Photo Album",
    dependencies: ["react", "react-icons", "fetch API"],
    description: "Photo album where you can slide through one by one",
    demoLink: "https://kavindu_l.gitlab.io/photo-album/",
    gitlabLink: "https://gitlab.com/kavindu_L/photo-album",
    date: "2022-11-8"
  },
  {
    id: 2,
    projectTitle: "Document Beautifier",
    dependencies: ["react", "react-to-pdf"],
    description:
      "To beautify the documents by changing alignment, font, color etc. and download a PDF",
    demoLink: "https://kavindu_l.gitlab.io/document-beautifier/",
    gitlabLink: "https://gitlab.com/kavindu_L/document-beautifier",
    date: "2022-11-8"
  },
  {
    id: 3,
    projectTitle: "The Blog",
    dependencies: ["react", "react-router-dom", "react-icons", "fetch API"],
    description: "Minimalist blog site concept",
    demoLink: "https://kavindu_l.gitlab.io/the-blog/",
    gitlabLink: "https://gitlab.com/kavindu_L/the-blog",
    date: "2022-11-8"
  },
  {
    id: 4,
    projectTitle: "Image Gallery",
    dependencies: ["react", "react-image-crop"],
    description: "An image gallery where you can edit the image",
    demoLink: "https://kavindu_l.gitlab.io/image-gallery/",
    gitlabLink: "https://gitlab.com/kavindu_L/image-gallery",
    date: "2022-11-15"
  },
  {
    id: 5,
    projectTitle: "Expense Tracker",
    dependencies: [
      "react",
      "react-redux",
      "redux",
      "react-circular-progressbar",
      "react-icons"
    ],
    description:
      "As a helping hand to track your expenses with real time dashboard",
    demoLink: "https://kavindu_l.gitlab.io/expense-tracker/",
    gitlabLink: "https://gitlab.com/kavindu_L/expense-tracker",
    date: "2022-11-28"
  },
  {
    id: 6,
    projectTitle: "Notice Board",
    dependencies: [
      "react",
      "react-redux",
      "reduxjs/toolkit",
      "react-router-dom",
      "material ui"
    ],
    description: "As a prototype for online notice board",
    demoLink: "https://kavindu_l.gitlab.io/notice-board",
    gitlabLink: "https://gitlab.com/kavindu_L/notice-board",
    date: "2023-02-03"
  }
];

export const dependencies = [
  "react",
  "react-icons",
  "fetch API",
  "react-to-pdf",
  "react-router-dom",
  "react-image-crop",
  "react-redux",
  "redux",
  "react-circular-progressbar",
  "reduxjs/toolkit",
  "material ui"
];
