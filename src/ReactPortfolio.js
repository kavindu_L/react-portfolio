import "./index.css";
import Hero from "./components/Hero/Hero";
import Projects from "./components/Projects/Projects";
import Contact from "./components/Contact/Contact";

function ReactPortfolio() {
  return (
    <div className="rp-app-wrapper">
      <Hero />
      <Projects />
      <Contact />
    </div>
  );
}

export default ReactPortfolio;
