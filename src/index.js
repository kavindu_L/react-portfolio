import React from "react";
import ReactDOM from "react-dom/client";
import ReactPortfolio from "./ReactPortfolio";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <ReactPortfolio />
  </React.StrictMode>
);
